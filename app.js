const latex = require('node-latex')
const stream = require('stream');
const fs = require('fs')
const base64 = require('base64-stream');
const latexfile = './templates/sura/main.tex';
const data = {
    lorem: 'Lorem ipsum dolor sit amet',
    name: 'David'
};


function addVariables(latexfile, commands) {
    return new Promise(resolve => {
        const input = fs.createReadStream(latexfile);
        const bufferStream = new stream.PassThrough();
        input.on('data', data => {
            let template = data.toString();
            let newCommands = generateNewCommands(commands);
            bufferStream.end(newCommands + template, () => {
                resolve(bufferStream);
            });
        });
    })
}

function generateNewCommands(commands) {
    let o = '';
    for (const key in commands) {
        o += `\\newcommand{\\${key}}{${commands[key]}}\n`
    }
    return o;
}

function generatePDF(bufferStream) {
    return new Promise((resolve, reject) => {
        const pdf = latex(bufferStream)
        let finalBufArray = [];
        pdf.pipe(base64.encode());
        pdf.on('error', err => reject(err));
        pdf.on('data', chunk => {
            finalBufArray.push(chunk);
        })
        pdf.on('end', () => {
            resolve(Buffer.concat(finalBufArray).toString('base64'));
            
        });
    })
}




addVariables(latexfile, data).then(bufferStream => {
    return generatePDF(bufferStream);
}).then(base64Data => {
    fs.writeFile('temp.txt', base64Data, () => {
        console.log('Sucess!');
    })
});
