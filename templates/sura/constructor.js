const mainTemplate = require('./main-template');
class template {
    constructor(name, vehicle) {
        this.name = name;
        this.vehicle = vehicle;
    }
    generate() {
        const {name, vehicle} = this;
        return mainTemplate({name, vehicle}) 
    }
}

module.exports = template;